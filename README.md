# Jeem
Jeem is a small flat-file php-only CMS in less than 400 lines of code.

Find out more at http://code-section.com/jeem


## License
Copyright (c) 2019 code-section Ltd

The license is subject to change without prior notice or consent.

Jeem is free for non-commercial use, and while it's still in Beta.