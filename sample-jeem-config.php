<?php

// The site name. Used by the default theme to display in the page header.
//$config["site-name"] = "Jeem";

// The theme to use for pages which do not explicitly specify a theme in their meta data section.
//$config["theme"] = "default";

// Directory names
//$config["pages-dir"] = "pages";
//$config["themes-dir"] = "themes";

// The file extensions for page files. Remove extensions you don't want to support.
//$config["page-extensions"] = ["php", "md", "html"];

// If true, Jeem will ignore pages which don't have explicit page menu assignment in the meta section.
//$config["require-menu"] = false;

// Controls whether or not Jeem will look for a <head> tag in the page contents and move it to the head section of the template.
//$config["extract-head"] = true;

// Format of the page meta data section. Jeem supports "INI" and "JSON".
//$config["meta-format"] = "INI";

// Tokens that mark the beginning and end of the page meta data section.
//$config["meta-opening-token"] = "---";
//$config["meta-closing-token"] = "---";

// This forces Jeem to use clean urls. Use this if you know url rewriting is working but Jeem can't detect it.
//$_ENV["JEEM_CLEAN_URLS"] = true;


// ---------------- Default theme configuration ---------------
//$config["site-name"] = "My Site";
//$config["theme-color"] = 'teal';

// Following configurations are used to embed various external services.
// Since some of these services use the domain name for identification,
// they are disabled by default while developing on localhost.
if ($_SERVER["SERVER_NAME"] != "localhost")
{
    //$config["disqus-embed-link"] = null; // Get from disqus.
    //$config["tawk.to.src"] = null; // Get from tawk.to.
    //$config["statcounter.src"] = null; // Get from statcounter.
}
// The following are used by the default theme.
$config["social-links"] = array( 'facebook' => '#', 'twitter' => '#', 'youtube' => '#', 'e-mail' => '#' );
$config["footer-links"] = array( 'About Us' => '#', 'Terms and Conditions' => '#', 'Privacy Policy' => '#', 'Cookies Policy' => '#', 'Advertise with Us' => '#');
?>