<?php

if( PHP_SAPI !== 'cli-server' )
    exit('Not supported outside CLI');

$_SERVER['JEEM_CLEAN_URLS'] = true;

if( is_file($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $_SERVER['SCRIPT_NAME']) )
    return false; // serve file as-is
/*
$_SERVER['SCRIPT_FILENAME'] = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'index.php';
$_SERVER['SCRIPT_NAME'] = DIRECTORY_SEPARATOR . 'index.php';
$_SERVER['PHP_SELF'] = DIRECTORY_SEPARATOR . 'index.php';
*/
require "index.php";