<?php
define( "JEEM_VERSION", "1.0 - beta" );
function PrintLine($msg) { file_put_contents('php://stderr', $msg."\n"); }
function PrintLineV($var) { file_put_contents('php://stderr', print_r($var, true) . "\n" ); }

$config = [];
$JEEM_CURRENT_PAGE = ""; // Contains the current page slug.

// Default configuration. 
$config["site-name"] = "Jeem";
$config["theme"] = "default";
$config["pages-dir"] = is_dir("pages") ? "pages" : "sample-pages";
$config["themes-dir"] = "themes";
$config["page-extensions"] = ["php", "md", "html"];
$config["require-menu"] = false;
$config["extract-head"] = true; // If set to false, Jeem will not look for the <head> tag to move it to the haed section.
$config["meta-format"] = "INI"; // Jeem supports INI, JSON.
$config["meta-opening-token"] = "---";
$config["meta-closing-token"] = "---";

// Load user preferences.
if( is_file( "jeem-config.php" ) )
	include( "jeem-config.php" );
else if( is_file("sample-jeem-config.php"))
	include( "sample-jeem-config.php");

$protocol = "http";
if( isset( $_SERVER["HTTPS"] ) && $_SERVER["HTTPS"] == "on" )
	$protocol = "https";

define( "SITE_NAME", $config['site-name']);
define( "BASE_URL", $protocol . "://" . $_SERVER["HTTP_HOST"] . str_replace( "index.php", "", $_SERVER["PHP_SELF"] ) );
define( "PAGES_DIR", $config["pages-dir"] );
define( "THEMES_DIR", $config["themes-dir"] );
define( "JEEM_CLEAN_URLS", (isset($_SERVER["JEEM_CLEAN_URLS"]) || isset($_ENV["JEEM_CLEAN_URLS"])) );
define( "BASE_PAGE_URL", BASE_URL . (JEEM_CLEAN_URLS ? "" : "?page=") );

function UniformSlashes( $s )
{
	if( DIRECTORY_SEPARATOR !== "/" )
		return str_replace( DIRECTORY_SEPARATOR, "/", $s );
	return $s;
}


// Returns the text between two tokens.
// $haystack: The string to search within
// $start, $end: The opening and closing tokens
// $inclusive: If set, the returned text will include the opening and closing tokens.
// $remove: If set, the function removes the text from the haystack.
function ExtractBetween( &$haystack, $start, $end, $inclusive=true, $remove=true, $replace_with="" ) {
	$i_start = strpos($haystack, $start); if ( $i_start === false ) return false;
	$i_end = strpos($haystack, $end, $i_start+strleN($start)); if( $i_end === false ) return false;
	$len = $i_end - $i_start + strlen($end);
	$ret = substr($haystack, $i_start, $len);
	if( !$inclusive )
		$ret = substr( $ret, strlen($start), -strlen($end) );
	if( $remove )
		$haystack = substr_replace( $haystack, $replace_with, $i_start, $len );
	return $ret;
}

function SlugFromName($name, $type ) {
	$idx = $type == "d" ? -1 : -2;
	$name = array_slice(explode(".", $name), $idx)[0];
	$name = str_replace(" ", "-", $name);
	if (function_exists("mb_strtolower"))
		return mb_strtolower($name);
	return strtolower($name);
}


// Calculates the slug from the given file path
// pages/4.blog/22.10.2018.my-blog-post.php => blog/my-blog-post
function SlugFromPath( $path ) {
	$path = UniformSlashes($path);
	$parts = [];
	foreach( explode("/", $path) as $p )
		if( $p ) $parts[] = $p;
	
	if( !$parts ) return null;
	
	if( $parts[0] == PAGES_DIR )
		array_shift($parts);

	$slug = "";
	for( $i=0; $i<count($parts)-1; $i++ )
		$slug .= ($slug ? "/" : "") . SlugFromName($parts[$i], "d");
	$file_slug = SlugFromName($parts[count($parts)-1], "f");
	if( $file_slug != "index" )
		$slug .= ($slug ? "/" : "") . $file_slug;
	return $slug;
}


// Scans a directory to determine the actual filename from the given slug.
// Returns null if not found.
function RealNameFromSlug($dir, $slug, $type) {
	global $config;
	if( $type == "f" && $slug == "" )
		$slug = "index";
	$path = $dir."/".$slug;
	if( $type == "d" && is_dir($path) ) return $slug;
	if( $type == "f" )
	{
		foreach( $config["page-extensions"] as $ext )
			if( is_readable("$path.$ext") )
				return $slug . "." . $ext;
	}
	
	foreach( scandir( $dir ) as $fn )
	{
		if( $fn == "." || $fn == ".." )//|| strpos($fn, $slug) === false ) 
			continue;
		$path = "$dir/$fn";
		if( $type == "f" && is_file($path) && in_array(pathinfo($path, PATHINFO_EXTENSION), $config['page-extensions']) && SlugFromName($fn, "f") == $slug )
			return $fn;
		if( $type == "d" && is_dir($path) && SlugFromName($fn, "d") == $slug )
			return $fn;
	}
	return null;
}


// Determines the actual path to the requested page.
// Returns null if not found.
function RealPathFromSlugPath( $dir, $slugPath ) {
	$parts = [];
	foreach(explode("/", $slugPath) as $p )
		if( $p ) $parts[] = $p;

	// Match all directories, stop when we have matched them all or stopped at the last part (the page slug)
	$matched = 0;
	foreach( $parts as $p )
	{
		$real_dir_name = RealNameFromSlug($dir, $p, "d" );
		if( !$real_dir_name )
			break;
		$dir .= "/" . $real_dir_name;
		$matched++;
	}
	$d = count($parts) - $matched;
	if( $d > 1) return null;
	if( $d == 0 ) $page_slug = "";
	if( $d == 1 ) $page_slug = $parts[$matched];
	$real_file_name = RealNameFromSlug($dir, $page_slug, "f" );
	return $real_file_name ? $dir."/".$real_file_name : null;
}


function WarningThrower($errno, $errstr) { throw new Exception($errstr); }


function ParseMetaData(&$page) {
	global $config;

	$meta_text = ExtractBetween($page['content'], $config["meta-opening-token"], $config["meta-closing-token"], false, true );
	if( !$meta_text )
		return;

	$meta = null;
	switch($config['meta-format']) {
		case "INI":
			set_error_handler( "WarningThrower", E_WARNING );
			try {
				$meta = parse_ini_string($meta_text, false, INI_SCANNER_RAW);
				foreach( $meta as $key=>$value ) {
					$v = strtolower($value);
					if(in_array($v, ['yes', 'true', 'on'])) $meta[$key] = true;
					else if (in_array($v, ['no', 'false', 'off'])) $meta[$key] = false;
				}
			}
			finally {restore_error_handler();}
			break;
		case "JSON":
			if( strpos( trim($meta_text), "{") !== 0 )
				$meta_text = "{ $meta_text }"; # Enclose json meta in {} if necessary
			$meta = json_decode($meta_text, true );
			if (json_last_error() != JSON_ERROR_NONE)
				throw new Exception( json_last_error_msg() );
			break;
		default:
			throw new Exception("Unknown meta format: " . $config['meta-format']);
	}
	if( $meta )
		$page = array_merge($page, $meta);
	if( !isset($page['title']) )
	{
		$h1_text = ExtractBetween($page['content'], "<h1>", "</h1>", false, false );
		if( $h1_text )
			$page['title'] = $h1_text;
	}
}


// Returns a page object from the specified file path.
// Returns null if the file is not a valid page file.
function PageFromFile( $filepath ) {
	global $config;
	global $JEEM_CURRENT_PAGE;
	static $s_pages = [];

	if( !is_file( $filepath ) )
		return null;
	
	$pathinfo = pathinfo( $filepath );
	if( !$pathinfo || !in_array($pathinfo["extension"], $config["page-extensions"]) )
		return null;

	$realpath = realpath( $filepath );
	if( isset( $s_pages[$realpath]) )
		return $s_pages[$realpath];

	$s_pages[$realpath] = [];
	$page = &$s_pages[$realpath];
	$page["filepath"] = $filepath;
	$page['date'] = date('F d Y', filemtime($filepath));
	$page["slug"] = SlugFromPath( $filepath );
	$page["current"] = ($page["slug"] == $JEEM_CURRENT_PAGE);
	$page["url"] = BASE_PAGE_URL . $page["slug"];
	$page["dir"] = $pathinfo["dirname"];
	$page["extension"] = strtolower($pathinfo["extension"]);
	$page["theme"] = $config["theme"];
	$ordinal_parts = array_slice( explode(".", $pathinfo["basename"]), 0, -2 );
	$page["ordinal"] = implode( ".", $ordinal_parts );
	$page["private"] = $page["hidden"] = false;
	if( $pathinfo["basename"][0] == "." )
		$page['private'] = true;
	else foreach( $ordinal_parts as $o )
	{
		if( strtolower($o) == "hidden" ) $page["hidden"] = true;
		if( strtolower($o) == "private" ) $page["private"] = true;
	}

	if( $page['private'] )
	{
		$page['content'] = "This page is private.";
		$page['hidden'] = True;
	}
	else
	{
		if( $page['extension'] == 'php' && $page['current'])
		{
			try {
				ob_start();
				require( $page["filepath"] );
				$page["content"] = ob_get_clean();
			}
			catch (Error $e) {
				#error_log( (string)$e );
				$page["content"] = str_replace( "\n", "\n<br>", (string)$e );
				$page["hidden"] = true;
			}
		}
		else
			$page['content'] = file_get_contents($page['filepath'] ); //$realpath
		
		// Extract meta data from page contents.
		try { ParseMetaData($page); }
		catch( Exception $e ) {
			$format = $config['meta-format'];
			$page["content"] = str_replace( "\n", "\n<br>", "<h2>Failed to parse $format page meta data</h2>" . (string)$e );
			$page["hidden"] = true;
		}
			
	}


	if( !isset($page["menu"]) )
	{
		if( $config["require-menu"] )
			return null;
		$slug_parts = explode("/", $page["slug"] ? $page["slug"] : "home" );
		$page["menu"] = ucwords(str_replace("-", " ", $slug_parts[ sizeof($slug_parts)-1 ] ) );
	}
	if( !isset($page['title']))
		$page['title'] = $page['menu'];

	$page["theme-dir"] = BASE_URL . THEMES_DIR . "/" . $page["theme"];

	return $page;
}


// Returns an array of the pages in the given directory sorted alphabetically.
// Hidden and private pages are not included.
// This function is not called internally - it can be called from the template to construct
// navigation menus or list blog posts for example.
function GetPages( $dir, $order=1 ) {
	$pages = [];
	$dir = UniformSlashes($dir);
	if( substr($dir, -1) != "/" )
		$dir .= "/";
	foreach( scandir( $dir, $order ) as $fn )
	{
		if( $fn == "." || $fn == ".." ) continue;

		$path = $dir . $fn;
		if( is_dir( $path ) ) // It's a directory, find the index 
			$path = RealPathFromSlugPath( $path, "" );
		if( !$path )
			continue;
		$page = PageFromFile( $path );
		if( !$page )
			continue;
		if( $page && !$page['hidden'])
			$pages[] = $page;
	}
	return $pages;
}


// Does some basic string replacements.
function ResolveJeemValues( &$page, $text ){
	$search = [];
	foreach($page as $key=>$value)
		$search[ "{{page.$key}}" ] = $value;
	$search = array_merge($search, [
		"{{PAGES_DIR}}" => PAGES_DIR,
		"{{THEMES_DIR}}" => THEMES_DIR,
		"{{BASE_URL}}" => BASE_URL,
		"{{BASE_PAGE_URL}}" => BASE_PAGE_URL,
		"{{JEEM_CLEAN_URLS}}" => JEEM_CLEAN_URLS ? 'true' : 'false',
		"{{JEEM_VERSION}}" => JEEM_VERSION,
		"{{SITE_NAME}}" => SITE_NAME
	]);
	return str_replace( array_keys($search), array_values($search), $text);
}


function LoadPageFromSlug( $slug ) {
	global $JEEM_CURRENT_PAGE;
	global $config;

	$path = RealPathFromSlugPath( PAGES_DIR, $slug );
	if( !$path )
		$path = RealPathFromSlugPath(PAGES_DIR, "404");
	
	$JEEM_CURRENT_PAGE = $slug;
	$page = PageFromFile( $path );

	if( !$page )
		die( "Error: Unable to get the specified page! path: $path");
	
	if( isset( $page["redirect"]) )
	{
		header('Location: '.BASE_PAGE_URL.$page['redirect']);
		die();
		#return LoadPageFromSlug($page["redirect"]);
	}

	//$page["content"] = ResolveJeemValues( $page, $page["content"] );

	if( $config["extract-head"] )
	{
		$head = ExtractBetween($page["content"], "<head>", "</head>", false, true);
		if( $head )
			$page["head"] = $head;
	}

	if( $page['extension'] == 'md' )
	{
		try {
			require "Parsedown.php";
			$PD = new Parsedown();
			$page["content"] = $PD->text($page["content"]);
		}
		catch (Error $e) {
			error_log( (string)$e );
			$page["content"] = str_replace( "\n", "\n<br>", (string)$e );
			$page["content"] = "Jeem: Make sure you have Parsedown.php to enable Markdown suppport.<br>" . $page["content"];
		}
	}
	
	ob_start();
	require( THEMES_DIR . "/" . $page["theme"] . "/" . "index.php" );
	$output = ob_get_clean();
	
	echo (ResolveJeemValues( $page, $output ));

	return true;
}


function Run() {
	$p = isset( $_GET["page"]) ? $_GET["page"] : "";
	//if( !$p && JEEM_CLEAN_URLS && isset( $_SERVER["PATH_INFO"]) )
	//    $p = $_SERVER["PATH_INFO"];
	$p = urlencode( $p );
	$p = filter_var( $p, FILTER_SANITIZE_URL );
	$p = urldecode( $p );

	LoadPageFromSlug( $p );
}
Run();
?>
