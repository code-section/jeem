<head>
    <style>
    .content-wrapper {
        max-width: 100%;
        padding: 0;
    }
    </style>
</head>


<div class="blog-posts">
<?php
foreach( GetPages( $page["dir"], 1 ) as $post )
{
    if( $page["url"] == $post["url"] )
        continue; // skip this index page.
?>
    <div class="post">
        <header>
            <a href="<?=$post["url"]?>"><?=$post["title"]?></a>
            <div class="date"><?php echo $post['date']; ?></div>
        </header>
        <div class="excerpt">
            <?php if( isset($post["excerpt"]) ) echo $post["excerpt"]; ?>
        </div>
    </div>
<?php }?>

</div>