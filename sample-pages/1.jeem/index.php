<!--
	---
	allow-comments = true
	description = Jeem is a tiny flat-file CMS written in PHP
	title = Jeem: the tiny flat-file PHP CMS
	theme-color = chocolate
	---
-->
<head>
	<!-- Jeem will remove the head tag from the body of the served page and place its contents inside the head tag in the template since a head tag is invalid inside the body tag. -->
	<!-- font awesome -->
	<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>

	<link rel="stylesheet" type="text/css" href="{{page.dir}}/style.css" >
</head>

<div class="jeem-header">
	<div>
		<header>Jeem: a tiny flat-file CMS</header>
		<div>
			<a href="https://bitbucket.org/code-section/jeem">Get Jeem</a>
			<a href="http://code-section.com/?page=discussions#!/jeem">Discuss</a>
			<a href="http://code-section.com/jeem">Homepage</a>
		</div>
	</div>
</div>
<div class="jeem-content-wrapper">
	<p>Jeem is a tiny flat-file <abbr title="Content Management System">CMS</abbr>. No database to mess with, no admin interface, no special syntax, twigs, plugins, or anything fancy. Simply create a file for each page and write your html/Markdown!</p>

	<p>Jeem is free for non-commercial use, and while it's in beta.</p>

	<div class="features">
		<div>
			<header><i class="fas fa-feather"></i>&nbsp; Lightweight</header>
			<p>Jeem is tiny and simple; in less than 400 lines of php code, it is lightweight and fast.</p>
		</div>

		<div>
			<header><i class="far fa-smile"></i>&nbsp; Easy</header>
			<p>Creating and editing pages is a matter of creating and editing html files. This page is all the documentation you will need!</p>
		</div>

		<div>
			<header><i class="fab fa-html5"></i>&nbsp; HTML, Markdown, and PHP</header>
			<p>What you write is what you get. The HTML is the language of the web, and of Jeem pages as well. Jeem also supports Markdown and PHP.</p>
		</div>

		<div>
			<header><i class="fab fa-php"></i>&nbsp; PHP Templates</header>
			<p>PHP is a templating language in the first place, so why not use it as such?</p>
		</div>

		<div>
			<header><i class="fas fa-code-branch"></i>&nbsp; Open Source</header>
			<p>Jeem is publicly available <a href="https://bitbucket.org/code-section/jeem">on BitBucket</a>. You are welcome to contribute.</p>
		</div>
	</div>

	<h2>Installing Jeem</h2>

	<p>Download Jeem and upload the extracted files to your server. Now you have a live website with sample pages!</p>

	<p>Since Jeem is a file-based CMS, there is no database to setup or configure. Backup, restoration, and deployment are simpler since your website is just a collection of files.</p>

	<p>Typically you will want to create and edit your website on your local machine and then upload it to your live server. You can use PHP's built-in server for local development. To upload your pages to your website, you can use an FTP client for example.</p>

	<h2>Creating Pages</h2>
	<p>You will need to create a directory called 'pages' containing an index page and a 404 (not found) page. Until you create it, Jeem will serve pages from the 'sample-pages' directory.</p>
	<p>The name of a page file determines its "slug", or the link which can be used to access the page. The file name has 3 parts: An ordinal, a title (or slug), and a file extension, all separated by dots <sup><abbr title="The last part is the extension, the one before it is the slug, and the rest is the ordinal part, which can have many dots"><i class="far fa-question-circle"></i></abbr></sup>. Consider this example:</p>

	<style>
		.extension { color: crimson; font-weight: bold;}
		.slug { color: green; font-weight: bold;}
		.ordinal { color: blue; font-weight: bold;}
	</style>
	<p class="center">
		<span class="ordinal">2019.08.02.</span><span class="slug">migrated-to-jeem</span>.<span class="extension">html</span>
	</p>

	<p><span class="ordinal">The ordinal part</span> is optional and does not affect the page link, but can be used to influence the order in which pages are listed, because Jeem always lists pages based on the alphabetical order of page file names. It can contain dots.</p>

	<p><span class="slug">The title or slug part</span> determines the link which can be used to access the page, as well as the page title if you don't explicitly specify a title.</p>

	<p>Jeem supports 3 <span class="extension">file extensions</span> for page files: html, md (Markdown), and php. Code in .php pages is executed when the page is displayed. Support for .php pages is one of the main advantages of using Jeem over a static site generator.</p>

	<p>The directory structure in the "pages" directory is reflected in the page links. If a subdirectory does not contain an index file, Jeem ignores it. Consider this table:</p>

	<table>
		<tr><th>Page File</th>      <th>Page Link</th></tr>
		<?php
			$files=[];
			#$files[] = "";
			$files[] = "index.php";
			$files[] = "0.index.php";
			$files[] = "my-page.php";
			$files[] = "1.my-page.php";
			$files[] = "news/index.php";
			$files[] = "news/2018.09.29.jeem-about-to-launch.php";
			$files[] = "news/00.archive/2017.02.01.old-news.php";
			foreach($files as $f)
			{
				$slug = SlugFromPath($f);
				echo "<tr><td>$f</td>    <td>{{BASE_PAGE_URL}}$slug</td></tr>\n";
			}
		?>
	</table>

	<h3 id="page-properties">Page Properties</h3>
	<p>You can assign any property you want to a page in the optional page properties section, then use those properties in your page template or the page itself. The properties section is enclosed by two tokens (default is ---). Example:</p>
<code><pre>
---
menu = Jeem
title = Jeem: the Tiny Flat-file CMS
date = July 12th, 2018
---
</pre></code>
	<p>Jeem understands the following page properties:</p>
	<ul>
		<li>menu: If not explicitly defined, Jeem sets it from the file name. The default theme uses it as the text for links to the page in navigation.</li>
		<li>title: If not explicitly defined, Jeem sets it from the contents of the first &lt;h1&gt; tag. If no &lt;h1&gt; tag was found, it is set to the 'menu' property. The default theme uses the page title property to set the &lt;title&gt; tag in the page head.</li>
		<li>description: If defined, the default theme uses it to create a meta description tag inside the &lt;head&gt; tag, which is useful for search engine visibility.</li>
		<li>theme: Defines the theme to use for the page. This overrides the theme you specify in the site's configuration file (jeem-config.php, see Configuring Jeem below).</li>
		<li>hidden: If set to true, the page will not show up in navigation links, but will remain accessible through direct links.</li>
		<li>private: If set to true, the page will not be accessible. Trying to access it will show a "This page is private" page. Implies 'hidden'.</li>
		<li>redirect: slug of a page to redirect to when this page is visited. Note that Jeem does nothing to ensure you have no cyclic redirects.</li>
	</ul>


	<h3>Jeem Values</h3>
	<p>Inside your page and theme, you can insert a jeem value from the "Name" column inside double curly braces {{&nbsp;}} (no spaces) and Jeem will substitute it with the actual value.</p>
	<table>
		<tr><th>Name</th><th>Value</th></tr>

		<tr><td>&#123;&#123;page.menu&#125;&#125;</td><td>{{page.menu}}</td></tr>
		<tr><td>&#123;&#123;page.title&#125;&#125;</td><td>{{page.title}}</td></tr>
		<tr><td>&#123;&#123;page.filepath&#125;&#125;</td><td>{{page.filepath}}</td></tr>
		<tr><td>&#123;&#123;page.slug&#125;&#125;</td><td>{{page.slug}}</td></tr>
		<tr><td>&#123;&#123;page.url&#125;&#125;</td><td>{{page.url}}</td></tr>
		<tr><td>&#123;&#123;page.dir&#125;&#125;</td><td>{{page.dir}}</td></tr>
		<tr><td>&#123;&#123;page.theme&#125;&#125;</td><td>{{page.theme}}</td></tr>
		<tr><td>&#123;&#123;PAGES_DIR&#125;&#125;</td><td>{{PAGES_DIR}}</td></tr>
		<tr><td>&#123;&#123;THEMES_DIR&#125;&#125;</td><td>{{THEMES_DIR}}</td></tr>
		<tr><td>&#123;&#123;BASE_URL&#125;&#125;</td><td>{{BASE_URL}}</td></tr>
		<tr><td>&#123;&#123;BASE_PAGE_URL&#125;&#125;</td><td>{{BASE_PAGE_URL}}</td></tr>
		<tr><td>&#123;&#123;JEEM_CLEAN_URLS&#125;&#125;</td><td>{{JEEM_CLEAN_URLS}}</td></tr>
		<tr><td>&#123;&#123;JEEM_VERSION&#125;&#125;</td><td>{{JEEM_VERSION}}</td></tr>
		<tr><td>&#123;&#123;SITE_NAME&#125;&#125;</td><td>{{SITE_NAME}}</td></tr>
	</table>

	<h3>PHP Pages</h3>

	<p>One of the main advantages of using a dynamic CMS instead of a static site generator is the ability to create "dynamic" sites. Jeem supports php pages and themes for this purpose.</p>

	<p>Jeem needs to read a page file for two reasons: extracting properties (for generating navigation links for example), and to actually display the page. Jeem does not execute php pages in the first case - it only executes them when they are being displayed.</p>

	<h2 id="configuring-jeem">Configuring Jeem</h2>
	<p>You can set custom configuration settings for Jeem by creating a file called "jeem-config.php" in the root directory. Use the sample-jeem-config.cfg file as reference. Uncomment the setting you want and modify its value:</p>
	<code><pre><?php echo htmlspecialchars(file_get_contents("{{PAGES_DIR}}/../sample-jeem-config.php")); ?></pre></code>

	<h3>Themes</h3>
	<p>Currently Jeem comes with two themes (or templates). You can select the active theme by changing <code>$config["theme"] = "default";</code> in jeem-config.php. You are encouraged to create your own theme however. To start, copy the "bare" or "default" directory in the "themes" directory, then rename it (Jeem uses the directory name as the theme name) and start modifying it. Jeem only requires an "index.php" file that will be the page template.</p>

	<h3>Clean URLs</h3>
	<p>You can enable clean urls in Jeem using the included .htaccess file for apache servers. This will allow you to access pages without the "?page=" part of the URL. If Jeem does not automatically detect that url rewriting is enabled, try uncommenting the line <code>$_ENV["JEEM_CLEAN_URLS"] = true;</code> in jeem-config.php.</p>

	<h2>Questions and Answers</h2>
	<?php include(".faq.html"); ?>

	<h3>Creating a Blog</h3>
	<div class="grid gutters">
		<div class="cell">
			<p>You can easily create a blog on your website thanks to the hierarchical nature of the file system. The screenshot shows how you would store your blog inside the "pages" directory.</p>
			<p>The ordinal for each blog post contains the post date in yyyy-mm-dd format to ensure that blog posts show up in chronological order.</p>
			<p>Some basic php knowledge is required to write the logic needed to list blog posts on the blog index page. You can use the included sample blog as reference.</p>
			<p>
			<code>Basic example: pages/blog/index.php</code>
<pre><code>&lt;?php
foreach( GetPages( $page[&quot;dir&quot;], 1 ) as $post )
{
	if( $page[&quot;url&quot;] == $post[&quot;url&quot;] )
		continue; // skip this index page.
?&gt;
	&lt;div class=&quot;post&quot;&gt;
		&lt;header&gt;
			&lt;a href=&quot;&lt;?=$post[&quot;url&quot;]?&gt;&quot;&gt;&lt;?=$post[&quot;title&quot;]?&gt;&lt;/a&gt;
			&lt;div class=&quot;date&quot;&gt;&lt;?php echo $post['date']; ?&gt;&lt;/div&gt;
		&lt;/header&gt;
		&lt;div class=&quot;excerpt&quot;&gt;
			&lt;?php if( isset($post[&quot;excerpt&quot;]) ) echo $post[&quot;excerpt&quot;]; ?&gt;
		&lt;/div&gt;
	&lt;/div&gt;
&lt;?php }?&gt;</code></pre>
			</p>
			<p>A similar setup can be used to group any set of related pages together, like news items for example. See the included sample pages.</p>
			<p>Jeem does not implement pagination yet.</p>
		</div>
		<div class="media">
			<figure>
				<img src="{{page.dir}}/blog-tree-ss.png" alt="blog file structure">
				<figcaption>Typical file structure for a website with a blog</figcaption>
			</figure>
		</div>
	</div>
	
	<h2>More Information</h2>
	<p>Visit <a href="http://code-section.com/jeem">code-section.com</a> for more information about Jeem.</p>
</div>