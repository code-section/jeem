<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.png"/>
    <title>{{page.title}} - {{SITE_NAME}}</title>
    <link rel="stylesheet" type="text/css" href="{{page.theme-dir}}/style.css" >
    <?php
        $theme_color = isset($page['theme-color']) ? $page['theme-color'] : null;
        if( !$theme_color ) $theme_color = isset($config['theme-color']) ? $config['theme-color'] : null;
        if( $theme_color )
            echo "<style>:root { --main-color: $theme_color; }</style>\n";
        $page_desc = isset($page['description']) ? $page['description'] : null;
        if( $page_desc )
            echo "<meta name=\"description\" content=\"$page_desc\">";
        if( isset( $page["head"]) )
            echo $page["head"];
    ?>
<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>
</head>
<body>
    <div class="body-wrapper" id="top-anchor">
        <header class="main">
            <a href="{{BASE_URL}}">{{SITE_NAME}}</a>
        </header>
        <nav class="main">
            <ul>
            <?php
            foreach( GetPages(PAGES_DIR, 0) as $p ) {
                $class = $p["current"] ? ' class="current"' : "";
                echo "
                <li><a href=\"$p[url]\"$class>$p[menu]</a></li>
                ";
            }?>
            </ul>
        </nav>
        <div class="main-wrapper">
            <div class="content-wrapper">
                <?php
                $note = [];
                if( isset($page['author'])) $note[] = $page['author'];
                if( false !== strpos( $page["slug"], "blog/" ) )
                    $note[] = $page['date'];
                if($note)
                    echo '<div><small><span class="note">' . implode(" | ", $note) . '</span></small></div>';

                echo $page["content"];

                // Show Disqus comments on blog entries and pages that set $page["allow-comments"].
                if( isset($config['disqus-embed-link']) &&
                    (isset($page["allow-comments"]) || false !== strpos( $page["slug"], "blog/" ) ) ){ ?>
                    <div id="disqus_thread"></div>
                    <script>
                        var disqus_config = function () {
                            this.page.url = "";
                            this.page.identifier = "{{page.slug}}";
                        };
                        
                        (function() { // DON'T EDIT BELOW THIS LINE
                            var d = document, s = d.createElement('script');
                            s.src = '<?php echo $config['disqus-embed-link']?>';
                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                        })();
                    </script>
                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                <?php } ?>
            </div>
        </div>

        <footer class="main">
            <div class="grid">
                <div class="cell">Powered by <a href="{{BASE_PAGE_URL}}jeem" title="{{JEEM_VERSION}}">Jeem</a></div>
                <div class="cell">
                    <?php
                        if( isset($config['footer-links']) )
                            foreach($config['footer-links'] as $text => $url)
                                echo "<a href=\"$url\">$text</a>\n";
                    ?>
                </div>
                <div class="cell social">
                    <ul>
                        <?php
                        $fb = isset($config['social-links']['facebook']) ? $config['social-links']['facebook'] : null;
                        $tw = isset($config['social-links']['twitter']) ? $config['social-links']['twitter'] : null;
                        $yt = isset($config['social-links']['youtube']) ? $config['social-links']['youtube'] : null;
                        $em = isset($config['social-links']['e-mail']) ? $config['social-links']['e-mail'] : null;
                        if($fb) echo "<li><a href=\"$fb\"><i class=\"fab fa-facebook\"></i></a></li>";
                        if($tw) echo "<li><a href=\"$tw\"><i class=\"fab fa-twitter\"></i></a></li>";
                        if($yt) echo "<li><a href=\"$yt\"><i class=\"fab fa-youtube\"></i></a></li>";
                        if($em) echo "<li><a href=\"$em\"><i class=\"fas fa-at\"></i></a></li>";
                        ?>
                        <li><a href="#top-anchor" title="Top"><i class="fas fa-arrow-up"></i></a></li>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
<?php if( isset($config['tawk.to.src'])) { ?>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var $_Tawk_API={},$_Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='<?php echo $config["tawk.to.src"]?>';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<?php } ?>

<?php if( isset($config['statcounter.src'])) { ?>
<!-- Default Statcounter code for Code-section.com
http://code-section.com -->
<script type="text/javascript">
var sc_project=8542458; 
var sc_invisible=1; 
var sc_security="45ad86c1"; 
</script>
<script type="text/javascript"
src="https://www.statcounter.com/counter/counter.js"
async></script>
<noscript><div class="statcounter"><a title="Web Analytics"
href="http://statcounter.com/" target="_blank"><img
class="statcounter"
src='<?php echo $config["statcounter.src"]; ?>' alt="Web Analytics"></a></div></noscript>
<!-- End of Statcounter Code -->

<?php } ?>

<!-- QA logic -->
<script>
questions = document.getElementsByClassName("question");
for( var i=0; i<questions.length; i++ )
    questions[i].onclick = function() { this.classList.toggle('collapsed'); };
</script>

</body>
</html>