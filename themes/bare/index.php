<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{SITE_NAME}} - {{page.title}}</title>
    <style>
        /*CSS: Eric Meyer Reset minified*/
        html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{border:0;font-size:100%;font:inherit;vertical-align:baseline;margin:0;padding:0}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:none}table{border-collapse:collapse;border-spacing:0}

        /* Some CSS rules for the sample pages */

        html { height: 100%; }
        body {
            --main-color: crimson;

            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 12pt;
            line-height: 16pt;
            min-height: 100%;
            overflow: hidden;
            display: flex;
            flex-direction: column;
        }
        header.main, footer.main {
            background-color: var(--main-color);
            color: white;
            padding: 0.75em;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }
        header.main { border-bottom: 2px solid black; }
        footer.main { border-top: 2px solid black; margin-top: auto; }
        header.main a, footer.main a { text-decoration: none; }
        #logo { font-size: 2em; color: white; text-shadow: 1px 1px 3px black; }

        nav.main ul { list-style: none; margin: 0.5em;}
        nav.main li { display: inline-block; }
        nav.main a { color: white; text-decoration: none; padding: 0.75em; display: inline-block; }
        nav.main a:hover, nav.main a.current { background: white; color: var(--main-color); transition: all 0.3s; }

        .content-wrapper { position: relative; margin: 2em 10%;}

        h1 {
            font-size: 48pt;
            white-space: nowrap;
            font-weight: bold;
            text-align: center;
            line-height: 1em;
            position: absolute;
            pointer-events: none;
            left: 0; right: 0;
            top: -1em;
            mix-blend-mode: difference;
            color: var(--main-color);
        }
        h2 { font-size: 22pt; line-height: 2.4em; color: var(--main-color); }
        h3 { font-size: 18pt; line-height: 2em; }
        h4 { font-weight: bold; line-height: 1.6em; }

        p { margin: 1em 0; }
        code { font-family: consolas; font-size: 10pt; }
        pre, blockquote { background: #e5e5e5; border-left: 3px solid var(--main-color); padding: 0.4em 1em; margin: 1em 0; }
        th { font-weight: bold; }
        td { padding: 0.5em 1.5em 0.5em 0; }
        table tr:nth-child(even) { background: rgba( 0,0,0,0.075); }
        table { min-width: 75%; margin: 0 auto; }
        img { max-width: 100%; }
    </style>
    <?php if( isset( $page["head"]) ) echo $page["head"]; ?>
</head>
<body>
    <header class="main">
        <a href="{{BASE_URL}}" id="logo">{{SITE_NAME}}</a>
        <nav class="main">
            <ul>
<?php foreach( GetPages(PAGES_DIR) as $p ) {
    $class = $p["current"] ? ' class="current"' : "";
    ?>
                <li><a href="<?=$p["url"]?>"<?=$class?>><?=$p["menu"]?></a></li>
<?php }?>
            </ul>
        </nav>
    </header>
    <div class="content-wrapper">
        <?php echo $page["content"]; ?>
    </div>
    <footer class="main">
        <div>Powered by <a href="http://code-section.com/jeem">Jeem</a></div>
    </footer>
</body>
</html>
